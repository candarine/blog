<?php

namespace AppBundle\Controller;

use AppBundle\Command\Command\PostDeleteCommand;
use AppBundle\Command\Exception\CommandHandlerException;
use AppBundle\Command\Handler\PostDeleteHandler;
use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use AppBundle\Command\Command\PostCreateCommand;
use AppBundle\Command\Handler\PostCreateHandler;
use AppBundle\Voter\PostVoter;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(PostVoter::POST_LIST);

        /** @var EntityRepository $repository */
        $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
        $posts = $repository->findBy(
            ['status' => Post::STATUS_PUBLIC],
            ['published' => 'DESC']
        );

        return $this->render('list.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * @Route("/create", name="post_create")
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $this->denyAccessUnlessGranted(PostVoter::POST_CREATE);

        $command = new PostCreateCommand();
        $command->user = $this->getUser();
        $form = $this->createForm(new PostType(), $command);
        $form->handleRequest($request);
        if ($form->isValid()) {
            try {
                /** @var PostCreateHandler $commandHandler */
                $commandHandler = $this->get('app.handler.post.create');
                $commandHandler->handle($command);
                $this->redirect('home');
            } catch (CommandHandlerException $e) {
                throw new ServiceUnavailableHttpException();
            }
        }

        return $this->render('create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/view", name="post_view")
     */
    public function viewAction(Post $post)
    {
        //todo: implement method
    }

    /**
     * @Route("/{id}/edit", name="post_edit")
     */
    public function editAction(Post $post)
    {
        //todo: implement method
    }

    /**
     * @Route("/{id}/delete", name="post_delete")
     * @Method("POST")
     * @param Post $post
     * @return RedirectResponse
     */
    public function deleteAction(Post $post)
    {
        $this->denyAccessUnlessGranted(PostVoter::POST_DELETE, $post);
        $command = new PostDeleteCommand();
        $command->post = $post;
        try {
            /** @var PostDeleteHandler $commandHandler */
            $commandHandler = $this->get('app.handler.post.delete');
            $commandHandler->handle($command);
        } catch (CommandHandlerException $e) {
            throw new ServiceUnavailableHttpException();
        }

        return  $this->redirect($this->generateUrl('home'));
    }
}