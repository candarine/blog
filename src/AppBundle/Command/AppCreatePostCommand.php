<?php

namespace AppBundle\Command;

use AppBundle\Command\Command\PostCreateCommand;
use AppBundle\Command\Exception\CommandHandlerException;
use AppBundle\Command\Handler\PostCreateHandler;
use AppBundle\Entity\Post;
use AppBundle\Login\LoginHelper;
use FOS\UserBundle\Security\LoginManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;

class AppCreatePostCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:create-post')
            ->setDescription('Create blog post')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');

        $login = $questionHelper->ask($input, $output, new Question('Login: '));
        $password = $questionHelper->ask($input, $output, new Question('Password: '));

        /** @var LoginHelper $loginHelper */
        $loginHelper = $this->getContainer()->get('app.helper.login');
        $currUser = $loginHelper->login($login, $password, 'main');

        $output->writeln("");

        $command = new PostCreateCommand();
        $command->title = $questionHelper->ask($input, $output, new Question('Post title: '));
        $command->body = $questionHelper->ask($input, $output, new Question('Post text: '));
        $command->status = $this->getStatus($questionHelper->ask($input, $output, new ChoiceQuestion('Post status: ', array('draft', 'public'), 0)));
        $command->user = $currUser;

        /** @var PostCreateHandler $handler */
        $handler = $this->getContainer()->get('app.handler.post.create');

        try {
            $handler->handle($command);
            $output->writeln("<info>Success</info>");
        } catch (CommandHandlerException $e) {
            $messages = implode("; ", $e->getMessages());
            $output->writeln("<error>Error: {$messages}</error>");
        }

        $output->writeln('Done');
    }

    private function getStatus($statusLabel)
    {
        return array_search($statusLabel, Post::getStatusNames());
    }

}
