<?php
namespace AppBundle\Command\Command;

use AppBundle\Entity\Post;
use Symfony\Component\Validator\Constraints as Assert;

class PostDeleteCommand
{
    /**
     * @var Post
     * @Assert\Type(type="AppBundle\Entity\Post")
     */
    public $post;
}