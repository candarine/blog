<?php
namespace AppBundle\Command\Handler;

use AppBundle\Command\Command\PostDeleteCommand;
use AppBundle\Command\Exception\CommandHandlerException;
use AppBundle\Model\PostModel;
use AppBundle\Voter\PostVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PostDeleteHandler
{
    /** @var AuthorizationCheckerInterface */
    private $authorizationChecker;

    /** @var ValidatorInterface */
    private $validator;

    /** @var PostModel */
    private $postModel;

    /**
     * PostCreateHandler constructor.
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param ValidatorInterface $validator
     * @param PostModel $postModel
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker, ValidatorInterface $validator, PostModel $postModel)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->validator = $validator;
        $this->postModel = $postModel;
    }

    /**
     * @param PostDeleteCommand $command
     */
    public function handle(PostDeleteCommand $command)
    {
        $this->denyAccessUnlessGranted(PostVoter::POST_DELETE, $command->post);
        $this->validate($command);

        $this->postModel->delete($command->post);
    }

    /**
     * @param $command
     * @throws CommandHandlerException
     */
    private function validate($command)
    {
        $errors = $this->validator->validate($command);
        if ($errors->count() > 0) {
            throw new CommandHandlerException("Validation error");
        }
    }

    /**
     * @param $attribute
     * @param $object
     * @return bool
     */
    private function isGranted($attribute, $object)
    {
        return $this->authorizationChecker->isGranted($attribute, $object);
    }

    /**
     * @param $attributes
     * @param null|mixed $object
     * @param string $message
     * @throws AccessDeniedException
     */
    protected function denyAccessUnlessGranted($attributes, $object = null, $message = 'Access Denied.')
    {
        if (!$this->isGranted($attributes, $object)) {
            throw new AccessDeniedException($message);
        }
    }
}