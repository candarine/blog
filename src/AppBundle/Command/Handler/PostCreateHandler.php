<?php

namespace AppBundle\Command\Handler;

use AppBundle\Entity\Post;
use AppBundle\Model\PostModel;
use AppBundle\Command\Command\PostCreateCommand;
use AppBundle\Command\Exception\CommandHandlerException;
use AppBundle\Voter\PostVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PostCreateHandler
{
    /** @var AuthorizationCheckerInterface */
    private $authorizationChecker;

    /** @var ValidatorInterface */
    private $validator;

    /** @var PostModel */
    private $postModel;

    /**
     * PostCreateHandler constructor.
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param ValidatorInterface $validator
     * @param PostModel $postModel
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker, ValidatorInterface $validator, PostModel $postModel)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->validator = $validator;
        $this->postModel = $postModel;
    }

    /**
     * @param PostCreateCommand $command
     */
    public function handle(PostCreateCommand $command)
    {
        $this->denyAccessUnlessGranted(PostVoter::POST_CREATE);
        $this->validate($command);

        $published = null;
        if ($command->status == Post::STATUS_PUBLIC) {
            $published = new \DateTime();
        }

        $this->postModel->create($command->user, $command->title, $command->body, $command->status, $published);
    }

    /**
     * @param $command
     * @throws CommandHandlerException
     */
    private function validate($command)
    {
        $errors = $this->validator->validate($command);
        if ($errors->count() > 0) {
            throw new CommandHandlerException("Validation error");
        }
    }

    /**
     * @param $attribute
     * @param $object
     * @return bool
     */
    private function isGranted($attribute, $object)
    {
        return $this->authorizationChecker->isGranted($attribute, $object);
    }

    /**
     * @param $attributes
     * @param null|mixed $object
     * @param string $message
     * @throws AccessDeniedException
     */
    protected function denyAccessUnlessGranted($attributes, $object = null, $message = 'Access Denied.')
    {
        if (!$this->isGranted($attributes, $object)) {
            throw new AccessDeniedException($message);
        }
    }
}